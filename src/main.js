import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import 'material-icons/iconfont/material-icons.css';
import VueSplide from '@splidejs/vue-splide';
import VueScrollTo from 'vue-scrollto';

Vue.config.productionTip = false
Vue.use(Vuesax, {
  // options here
});

Vue.use( VueSplide );
Vue.use(VueScrollTo);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
